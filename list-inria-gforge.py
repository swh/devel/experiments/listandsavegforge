#!/usr/bin/env python3
import sys
from zeep import Client
import getpass
import requests
import os.path
from os import path

#
# Load bearer token to bypass rate limit
#
if os.path.isfile("swhtoken"):
  with open('swhtoken', 'r') as file:
    TOKEN = file.read().replace('\n', '')
headers = {'Authorization': f"Bearer {TOKEN}",}

#
# code from Sebastien Gilles
#
c=Client("https://gforge.inria.fr/soap/index.php?wsdl")    
user=input("gforge login: ")
password=getpass.getpass()
s=c.service.login(user,password)

names=c.service.getPublicProjectNames(s)
t=c.get_type("ns0:ArrayOfstring")
print(f"# found {len(names)} visible projects")
for name in names:
  project_info=c.service.getGroupsByName(s,t([name]))[0] # using names to get the whole array directly does not work
  pid=project_info["group_id"]
  if project_info["use_scm"]=="1":
    roles=c.service.getGroupRoles(s,pid)
    # check if "anonymous (=not logged in)" has scm access
    anonymous_has_scm_access=False
    for role in roles:
      if role["role_id"]==1:
        assert role["role_name"]=="Anonymous", f"project {name} : role_id 1 is not anonymous ???"
        anonymous_has_scm_access=(role["scm"]!=0) # TODO: &1 instead ?
    if anonymous_has_scm_access:
      scm_data=c.service.getSCMData(s,pid)
      if scm_data["type"]=="SVN":
        # svn://scm.gforge.inria.fr/svnroot/orccad/
        # TODO: check (buggy) scm_data["connection_string"] and compare to pure guess
        print(name,pid,f"svn://scm.gforge.inria.fr/svnroot/{name}/")
        response=requests.post(f"https://archive.softwareheritage.org/api/1/origin/save/svn/url/svn://scm.gforge.inria.fr/svnroot/{name}/",headers=headers)
        print(response.text)
      else: # for git it seems all fields are "None"
        #https://scm.gforge.inria.fr/anonscm/git/cado-nfs/cado-nfs.git
        print(name,pid,f"https://scm.gforge.inria.fr/anonscm/git/{name}/{name}.git")
        response=requests.post(f"https://archive.softwareheritage.org/api/1/origin/save/git/url/https://scm.gforge.inria.fr/anonscm/git/{name}/{name}.git/",headers=headers)
        print(response.text)
    else:
      print(f"# {name} {pid} scm with no anonymous access")
  else:
    print(f"# {name} {pid} no scm")

c.service.logout(s)
